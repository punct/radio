import { createAction } from 'redux-actions'

export const CREATE_STATION = 'CREATE_STATION'
export const CREATE_STATION_FAILED = 'CREATE_STATION_FAILED'
export const CREATE_STATION_SUCCESS = 'CREATE_STATION_SUCCESS'

export const createStation = createAction(CREATE_STATION)
export const createStationFailed = createAction(CREATE_STATION_FAILED)
export const createStationSuccess = createAction(CREATE_STATION_SUCCESS)
