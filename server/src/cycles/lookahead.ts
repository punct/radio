import xs, { Stream } from 'xstream'
import sampleCombine from 'xstream/extra/sampleCombine'

const lookahead = sources => {
  const state$ = sources.STATE

  const lookaheadRequest$: Stream<any> = sources.WS.filter(message => message.type === 'LOOKAHEAD_REQUEST')
    .compose(sampleCombine(state$))
    .map(([request, state]) => ({
      url: `https://c${state.api.CLIENT_ID}.web.cddbp.net/webapi/json/1.0/radio/lookahead`,
      query: {
        client: `${state.api.CLIENT_ID}-${state.api.CLIENT_TAG}`,
        user: state.api.USER_ID,
        radio_id: request.payload.station,
        return_count: 10
      },
      category: 'lookahead',
      initiator: request.initiator
    }))
    
  const lookaheadResponse$ = sources.HTTP.select('lookahead').flatten()
    .map(response => ({
      type: 'LOOKAHEAD_SUCCESS',
      payload: {
        id: response.body.RESPONSE[0].RADIO[0].ID,
        playlist: response.body.RESPONSE[0].ALBUM.map(entry => ({
          id: entry.GN_ID,
          artist: entry.ARTIST[0].VALUE,
          track: entry.TRACK[0].TITLE[0].VALUE,
          album: entry.TITLE[0].VALUE
        }))
      },
      meta: {
        initiator: response.request.initiator
      }
    }))

  return {
    ACTION: lookaheadResponse$,
    HTTP: lookaheadRequest$,
    WS: lookaheadResponse$.map(response => ({
      data: {
        type: response.type,
        payload: response.payload
      },
      destination: response.meta.initiator
    }))
  }
}

export default lookahead
