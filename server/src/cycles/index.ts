import xs, { Stream } from 'xstream'
import { HTTPSource, RequestInput as HTTPRequestInput } from '@cycle/http'

import { connectOutput } from '../lib/utils'

import fieldvalues from './fieldvalues'
import create from './create'
import lookahead from './lookahead'
import setting from './setting'
import event from './event'
import initializer from './initializer'

export type Action = {
  type: string
  payload?: object
  meta?: object
}

export type Sources = {
  ACTION: Stream<Action>
  HTTP: HTTPSource
  STATE: Stream<any>
  WS: Stream<any>
}

export type Sinks = {
  ACTION?: Stream<Action>
  HTTP?: Stream<HTTPRequestInput>,
  WS?: Stream<any>
}

function main (sources: Sources): Sinks {
  sources.STATE = sources.STATE.remember()

  const initializer$ = initializer(sources)
  const fieldvalues$ = fieldvalues(sources)
  const create$ = create(sources)
  const lookahead$ = lookahead(sources)
  const setting$ = setting(sources)
  const event$ = event(sources)
  
  return connectOutput(['ACTION', 'HTTP', 'WS'])([
    initializer$,
    fieldvalues$,
    create$,
    lookahead$,
    setting$,
    event$
  ])
}

export default main
