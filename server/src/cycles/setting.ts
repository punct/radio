import xs, { Stream } from 'xstream'
import sampleCombine from 'xstream/extra/sampleCombine'

const setting = sources => {
  const state$ = sources.STATE

  const settingRequest$: Stream<any> = sources.WS.filter(message => message.type === 'SETTING_REQUEST')
    .compose(sampleCombine(state$))
    .map(([request, state]) => {
      let settings = {
        filter_mood: (request.payload.moods && request.payload.moods.join(',')) || null,
        focus_popularity: request.payload.popularity,
        focus_similarity: request.payload.similarity,
      }
      settings = Object.entries(settings)
        .filter(([key, value]) => value)
        .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
      
      return {
        url: `https://c${state.api.CLIENT_ID}.web.cddbp.net/webapi/json/1.0/radio/setting`,
        query: {
          ...settings,
          client: `${state.api.CLIENT_ID}-${state.api.CLIENT_TAG}`,
          user: state.api.USER_ID,
          radio_id: request.payload.station,
          return_count: 10
        },
        category: 'setting',
        initiator: request.initiator
      }
    })
    .filter(() => false)
    
  const settingResponse$ = sources.HTTP.select('setting').flatten()
    .map(response => ({
      type: 'SETTING_SUCCESS',
      payload: {
        id: response.body.RESPONSE[0].RADIO[0].ID,
        playlist: response.body.RESPONSE[0].ALBUM.map(entry => ({
          artist: entry.ARTIST[0].VALUE,
          track: entry.TRACK[0].TITLE[0].VALUE,
          album: entry.TITLE[0].VALUE
        }))
      },
      meta: {
        initiator: response.request.initiator
      }
    }))

  return {
    ACTION: settingResponse$,
    HTTP: settingRequest$,
    WS: settingResponse$.map(response => ({
      data: {
        type: response.type,
        payload: response.payload
      },
      destination: response.meta.initiator
    }))
  }
}

export default setting
