import xs, { Stream } from 'xstream'
import sampleCombine from 'xstream/extra/sampleCombine'

const event = sources => {
  const state$ = sources.STATE

  const eventRequest$: Stream<any> = sources.WS.filter(message => message.type === 'EVENT_REQUEST')
    .compose(sampleCombine(state$))
    .map(([request, state]) => ({
      url: `https://c${state.api.CLIENT_ID}.web.cddbp.net/webapi/json/1.0/radio/event`,
      query: {
        client: `${state.api.CLIENT_ID}-${state.api.CLIENT_TAG}`,
        user: state.api.USER_ID,
        radio_id: request.payload.station,
        event: `${request.payload.type}_${request.payload.entity}`,
        return_count: 10
      },
      category: 'event',
      initiator: request.initiator
    }))
    
  const eventResponse$ = sources.HTTP.select('event').flatten()
    .map(response => ({
      type: 'EVENT_SUCCESS',
      payload: {
        id: response.body.RESPONSE[0].RADIO[0].ID,
        playlist: response.body.RESPONSE[0].ALBUM.map(entry => ({
          artist: entry.ARTIST[0].VALUE,
          track: entry.TRACK[0].TITLE[0].VALUE,
          album: entry.TITLE[0].VALUE
        }))
      },
      meta: {
        initiator: response.request.initiator
      }
    }))

  return {
    ACTION: eventResponse$,
    HTTP: eventRequest$,
    WS: eventResponse$.map(response => ({
      data: {
        type: response.type,
        payload: response.payload
      },
      destination: response.meta.initiator
    }))
  }
}

export default event
