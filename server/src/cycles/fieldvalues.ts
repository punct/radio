import xs, { Stream } from 'xstream'
import sampleCombine from 'xstream/extra/sampleCombine'

import { connectOutput } from '../lib/utils'
import { Sources, Sinks, Action } from './index'

const moods = (sources: Sources): Sinks => {
  const state$ = sources.STATE

  const getMoodsResponse$: Stream<any> = sources.HTTP.select('moods').flatten()
    .map(response => {
      return {
        payload: response.body.RESPONSE[0].MOOD.map(mood => ({
          id: mood.ID,
          name: mood.VALUE
        })),
        request: response.request
      }
    })
    .map(response => ({
      data: {
        type: 'MOODS_RESPONSE',
        payload: response.payload
      },
      destination: response.request.initiator
    }))

  const getMoodsRequest$ = sources.WS.filter(message => message.type === 'MOODS_REQUEST')
    .compose(sampleCombine(state$))
    .map(([request, state]) => ({
      url: `https://c${state.api.CLIENT_ID}.web.cddbp.net/webapi/json/1.0/radio/fieldvalues`,
      query: {
        client: `${state.api.CLIENT_ID}-${state.api.CLIENT_TAG}`,
        user: state.api.USER_ID,
        fieldname: 'RADIOMOOD'
      },
      category: 'moods',
      initiator: request.initiator
    }))

  return {
    HTTP: getMoodsRequest$,
    WS: getMoodsResponse$,
  }
}

const eras = (sources: Sources): Sinks => {
  const state$ = sources.STATE

  const getErasResponse$: Stream<any> = sources.HTTP.select('eras').flatten()
    .map(response => ({
      data: {
        type: 'ERAS_RESPONSE',
        payload: response.body.RESPONSE[0].ERA
      },
      destination: response.request.initiator
    }))

  const getErasRequest$ = sources.WS.filter(message => message.type === 'ERAS_REQUEST')
    .compose(sampleCombine(state$))
    .map(([request, state]) => ({
      url: `https://c${state.api.CLIENT_ID}.web.cddbp.net/webapi/json/1.0/radio/fieldvalues`,
      query: {
        client: `${state.api.CLIENT_ID}-${state.api.CLIENT_TAG}`,
        user: state.api.USER_ID,
        fieldname: 'RADIOERA'
      },
      category: 'eras',
      initiator: request.initiator
    }))

  return {
    HTTP: getErasRequest$,
    WS: getErasResponse$
  }
}

const genres = (sources: Sources): Sinks => {
  const state$ = sources.STATE

  const getGenreResponse$: Stream<any> = sources.HTTP.select('genres').flatten()
    .map(response => ({
      data: {
        type: 'GENRE_RESPONSE',
        payload: response.body.RESPONSE[0].GENRE
      },
      destination: response.request.initiator
    }))

  const getGenreRequest$ = sources.WS.filter(message => message.type === 'GENRE_REQUEST')
    .compose(sampleCombine(state$))
    .map(([request, state]) => ({
      url: `https://c${state.api.CLIENT_ID}.web.cddbp.net/webapi/json/1.0/radio/fieldvalues`,
      query: {
        client: `${state.api.CLIENT_ID}-${state.api.CLIENT_TAG}`,
        user: state.api.USER_ID,
        fieldname: 'RADIOGENRE'
      },
      category: 'genres',
      initiator: request.initiator
    }))

  return {
    HTTP: getGenreRequest$,
    WS: getGenreResponse$
  }
}

const fieldvalues = (sources: Sources): Sinks => {
  const moods$ = moods(sources)
  const eras$ = eras(sources)
  const genres$ = genres(sources)

  return connectOutput(['ACTION', 'HTTP', 'WS'])([
    moods$,
    eras$,
    genres$
  ])
}

export default fieldvalues
