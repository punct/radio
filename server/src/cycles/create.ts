import xs, { Stream } from 'xstream'
import sampleCombine from 'xstream/extra/sampleCombine'
import delay from 'xstream/extra/delay'

import { connectOutput } from '../lib/utils'
import { Sources, Sinks, Action } from './index'

const create = (sources: Sources): Sinks => {
  const state$ = sources.STATE

  const createStationRequest$: Stream<any> = sources.WS.filter(message => message.type === 'CREATE_STATION_REQUEST')
    .compose(sampleCombine(state$))
    .map(([request, state]) => ({
      url: `https://c${state.api.CLIENT_ID}.web.cddbp.net/webapi/json/1.0/radio/create`,
      query: {
        client: `${state.api.CLIENT_ID}-${state.api.CLIENT_TAG}`,
        user: state.api.USER_ID,
        seed: request.payload.seeds
          .map(seed => `(${seed})`)
          .join(';')
      },
      category: 'createStation',
      initiator: request.initiator,
      extra: {
        name: request.payload.name
      }
    }))
    
  const createStationResponse$ = sources.HTTP.select('createStation').flatten()
    .map(response => ({
      type: 'CREATE_STATION_SUCCESS',
      payload: {
        id: response.body.RESPONSE[0].RADIO[0].ID,
        name: response.request.extra.name,
        playlist: response.body.RESPONSE[0].ALBUM.map(entry => ({
          artist: entry.ARTIST[0].VALUE,
          track: entry.TRACK[0].TITLE[0].VALUE,
          album: entry.TITLE[0].VALUE
        }))
      },
    }))

  return {
    ACTION: createStationResponse$,
    HTTP: createStationRequest$,
    WS: createStationResponse$.map(response => ({
      data: response,
      broadcast: true
    }))
  }
  // return connectOutput(['ACTION', 'HTTP', 'WS'])()
}

export default create
