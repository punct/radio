import xs, { Stream } from 'xstream'
import sampleCombine from 'xstream/extra/sampleCombine'

const initializer = sources => {
  const state$ = sources.STATE

  const initializerRequest$: Stream<any> = sources.WS.filter(message => message.type === 'INITIALIZE_REQUEST')
    .compose(sampleCombine(state$))
    .map(([request, state]) => ({
      state,
      request
    }))
    
  const initializerResponse$ = initializerRequest$
    .map(response => ({
      type: 'INITIALIZE_RESPONSE',
      payload: {
        stations: response.state.stations,
      },
      meta: {
        initiator: response.request.initiator
      }
    }))

  return {
    WS: initializerResponse$.map(response => ({
      data: {
        type: response.type,
        payload: response.payload
      },
      destination: response.meta.initiator
    }))
  }
}

export default initializer
