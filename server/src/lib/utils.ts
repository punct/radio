import xs from 'xstream'

export const connectOutput = fields => streams => {
  return fields.map(fieldName => ({
    fieldName,
    stream: xs.merge.apply(xs, streams.map(stream => stream[fieldName] || xs.never()))
  }))
    .reduce((acc, output) => ({ ...acc, [output.fieldName]: output.stream }), {})
}
