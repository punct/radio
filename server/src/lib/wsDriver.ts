import xs, { Stream } from 'xstream'
import {adapt} from '@cycle/run/lib/adapt'

const crypto = require('crypto')
const ws = require('ws')

export type WSDriverOptions = {
  port: number,
  host: string
}

export type WSDriverOutgoing = {
  broadcast?: boolean
  destination: any
  data: any
}

const makeWSDriver = (options: WSDriverOptions) => (outgoing$: Stream<WSDriverOutgoing>) => {
  let serverReady = false
  let clients = {}

  const server = new ws.Server({
    port: options.port
  })
  server.broadcast = function broadcast(data) {
    server.clients.forEach(function each(client) {
      if (client.readyState === ws.OPEN) {
        client.send(data);
      }
    });
  };
  server.on('listening', () => {
    serverReady = true
  })

  outgoing$.addListener({
    next: outgoing => {
      if (!serverReady) {
        console.error('Server not ready:', outgoing)
      }

      const payload = JSON.stringify(outgoing.data)

      if (outgoing.broadcast) {
        return server.broadcast(payload)
      }
      if (!outgoing.destination) {
        return console.error('Destination missing:', outgoing)
      }
      if (outgoing.destination) {
        if (clients[outgoing.destination]) {
          return clients[outgoing.destination].send(payload)
        } else {
          return console.error('Dropping message due to socket disconnection', outgoing)
        }
      } else {
        console.error('Unknown message:', outgoing)
      }
    }
  })
  const incoming$ = xs.create({
    start: listener => {
      server.on('connection', ws => {
        ws.id = crypto.randomBytes(32).toString('hex')
        console.log(`[server] client connected: ${ws.id}`);
        
        clients = {
          ...clients,
          [ws.id]: ws
        }
        ws.on('message', msg => {
          const decodedMessage = JSON.parse(msg)
          try {
            listener.next({
              ...decodedMessage,
              initiator: ws.id
            })
          } catch (error) {
            console.error('Error parsing message:', error)
          }
        })
        ws.on('close', () => {
          const { [ws.id]: current, ...others } = clients
          clients = others
          console.log(`[server] client disconected: ${ws.id}`);
        })
      });
    },
    stop: () => {
      console.log('stop incoming')
    },
  })
  return adapt(incoming$)
}

export default makeWSDriver
