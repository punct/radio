import configureStore from './configureStore'
import reducers from './reducers'
import main from './cycles/'

const chalk = require('chalk')
const ws = require('ws')

const store = configureStore(reducers, main)()
store.subscribe(() => {
  console.log(chalk.blue('state'), store.getState())
})

// setTimeout(() => {
//   const client = new ws(`ws://localhost:8080`)
//   client.on('open', () => {
//     console.log(chalk.bgGreen.black('[client] connected'))
//     // setTimeout(() => client.send(JSON.stringify({
//     //   type: 'CREATE_STATION_REQUEST',
//     //   payload: {
//     //     name: 'test_playlist',
//     //     seeds: ['text_artist_led+zeppelin', 'genre_36060', 'mood_42953']
//     //   }
//     // })), 0)
//     // setTimeout(() => client.send(JSON.stringify({
//     //   type: 'LOOKAHEAD_REQUEST',
//     //   payload: {
//     //     station: '57d9591a90318e000e9065c3f0721cd6'
//     //   }
//     // })), 0)
//     // setTimeout(() => client.send(JSON.stringify({
//     //   type: 'SETTING_REQUEST',
//     //   payload: {
//     //     station: '57d9591a90318e000e9065c3f0721cd6',
//     //     popularity: Math.floor(Math.random() * 1000 + 1)
//     //   }
//     // })), 0)
//     // setTimeout(() => client.send(JSON.stringify({
//     //   type: 'EVENT_REQUEST',
//     //   payload: {
//     //     station: '57d9591a90318e000e9065c3f0721cd6',
//     //     type: 'track_skipped',
//     //     entity: '168384139-03ED4FB15AFF89B6085BB760AF4D042D'
//     //   }
//     // })), 0)
//     setTimeout(() => client.send(JSON.stringify({
//       type: 'INITIALIZE_REQUEST',
//       payload: {}
//     })), 0)
//     // setTimeout(() => client.send(JSON.stringify({ type: 'GENRE_REQUEST' })), 0)
//     // setTimeout(() => client.send(JSON.stringify({ type: 'ERAS_REQUEST' })), 1000)
//     // setTimeout(() => client.send(JSON.stringify({ type: 'MOODS_REQUEST' })), 2000)
//     // setTimeout(() => client.send)
//     setTimeout(() => client.close(), 3000)
//   })
//   client.on('message', data => {
//     console.log(chalk.bgGreen.black('[client] received:'), data)
//   })
// }, 1000)
