import { combineReducers } from 'redux'
import { handleActions } from 'redux-actions'

import {
  CREATE_STATION_SUCCESS
} from './actions'

const stationsReducers = handleActions({
  [CREATE_STATION_SUCCESS]: (prevState, action) => {
    const station = {
      id: action.payload.id,
      name: action.payload.name,
      seeds: action.payload.seeds,
    }
    return [...prevState, station]
  }
}, [])

const apiReducer = handleActions({}, {
  CLIENT_ID: '1715984121',
  CLIENT_TAG: '709963A47B7EAE1779C0634491F57420',
  USER_ID: '279884734615283631-FC7A15118252C6B3EA63BFED8FBBECE1'
})

export default combineReducers({
  api: apiReducer,
  stations: stationsReducers
})
