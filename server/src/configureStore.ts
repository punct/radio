import { createStore, applyMiddleware, Middleware } from 'redux'
import { createCycleMiddleware } from 'redux-cycles'
import { run } from '@cycle/run'
import { makeHTTPDriver } from '@cycle/http'

import makeWSDriver from './lib/wsDriver'

const fs = require('fs')
const chalk = require('chalk')


const createPersistenceMiddleware = opts => {
  const initialState = JSON.parse(fs.readFileSync(opts.file).toString())

  const persistenceMiddleware = store => next => action => {
    const x = next(action)
    const state = JSON.stringify(store.getState())
    fs.writeFileSync(opts.file, state)
    return x
  }

  return {
    persistenceMiddleware,
    initialState
  }
}

const createDebugMiddleware = () => {
  return store => next => action => {
    console.log(chalk.yellow('dispatched'), action.type, action.payload)
    return next(action)
  }
}

const configureStore = (reducers, main) => () => {
  const cycleMiddleware = createCycleMiddleware()
  const { makeActionDriver, makeStateDriver } = cycleMiddleware

  const { persistenceMiddleware, initialState } = createPersistenceMiddleware({ file: 'storage.json' })
  
  const store = createStore(
    reducers,
    initialState,
    applyMiddleware(
      createDebugMiddleware(),
      persistenceMiddleware,
      cycleMiddleware
    )
  )

  run(main, {
    ACTION: makeActionDriver(),
    STATE: makeStateDriver(),
    HTTP: makeHTTPDriver(),
    WS: makeWSDriver({ port: 8080, host: '0.0.0.0' })
  })

  return store
}

export default configureStore
