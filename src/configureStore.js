import { createStore, compose, applyMiddleware } from 'redux'
import { createCycleMiddleware } from 'redux-cycles'
import { run } from '@cycle/run'

import makeWSDriver from './lib/wsDriver'
import reducers from './reducers'
import main from './cycle'

const actionLog = () => store => next => action => {
  // console.log('action', action)
  return next(action)
}

const configureStore = (initialState = {}) => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  const cycleMiddleware = createCycleMiddleware()
  const { makeActionDriver, makeStateDriver } = cycleMiddleware

  const store = createStore(reducers, initialState, composeEnhancers(
    applyMiddleware(
      actionLog(),
      cycleMiddleware
    )
  ))

  run(main, {
    ACTION: makeActionDriver(),
    STATE: makeStateDriver(),
    WS: makeWSDriver({ url: 'ws://localhost:8080' })
  })

  return store
}

export default configureStore
