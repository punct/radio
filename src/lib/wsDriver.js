import xs from 'xstream'
import { adapt } from '@cycle/run/lib/adapt'

export const CONNECTION_OPEN = 'CONNECTION_OPEN'

const makeWSDriver = ({ url }) => {
  const client = new window.WebSocket(url)
  let connectionOpened = false

  client.addEventListener('open', () => {
    connectionOpened = true
  })
  client.addEventListener('close', () => {
    connectionOpened = false
  })
  const sockDriver = outgoing$ => {
    outgoing$.addListener({
      next: outgoing => {
        if (connectionOpened) {
          const message = JSON.stringify(outgoing)
          client.send(message)
        } else {
          console.warn('Client not connected, dropping message', outgoing)
        }
      },
      error: error => console.error('WSDriver', error),
      complete: () => console.warn('WSDriver complete')
    })

    const incoming$ = xs.create({
      start: listener => {
        client.addEventListener('open', () => {
          listener.next({ type: CONNECTION_OPEN })
        })
        client.addEventListener('message', msg => {
          try {
            listener.next(JSON.parse(msg.data))
          } catch (error) {
            console.error('Error parsing', error)
          }
        })
      },
      stop: () => {}
    })

    return adapt(incoming$)
  }

  return sockDriver
}

export default makeWSDriver
