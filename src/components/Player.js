import React from 'react'

import b from './b.mp3'

const Player = props => {
  return (
    <div className='player'>
      <audio controls='controls'>
        <source src={b} type='audio/mp3' />
        Your browser does not support the <code>audio</code> element.
      </audio>
    </div>
  )
}

export default Player
