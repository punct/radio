import React, { Component } from 'react'
import { TextField, Popover, Menu, MenuItem } from 'material-ui'

class AutoComplete extends Component {
  constructor (props) {
    super(props)

    this.onSearchBarFocus = this.onSearchBarFocus.bind(this)
    this.onSearchBarBlur = this.onSearchBarBlur.bind(this)
    this.onSeedSelect = this.onSeedSelect.bind(this)

    this.state = {
      open: false
    }
  }

  onSearchBarFocus (event) {
    this.setState({
      open: true,
      anchorEl: event.currentTarget
    })
  }

  onSearchBarBlur () {
    this.setState({ open: false })
  }

  onSeedSelect (event, menuItem) {
    const id = parseInt(menuItem.key, 10)
    const selectedItem = this.props.dataSource
      .filter(data => data.id === id)
      .reduce((acc, data) => data, null)

    this.props.onItemSelect(selectedItem)
    this.onSearchBarBlur()
  }

  render () {
    return (
      <div className='autocomplete'>
        <TextField
          hintText={this.props.hintText}
          floatingLabelText={this.props.floatingLabelText}
          onFocus={this.onSearchBarFocus}
          // onBlur={this.onSearchBarBlur}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          onRequestClose={this.onSearchBarBlur}
          anchorOrigin={{ vertical: 'center', horizontal: 'left' }}
          targetOrigin={{ vertical: 'top', horizontal: 'left' }}
          style={{ marginTop: '15px' }}
        >
          <Menu
            disableAutoFocus
            onItemClick={this.onSeedSelect}
          >
            {
              this.props.dataSource.map(data =>
                <MenuItem
                  key={data.id}
                  value={data.id}
                >
                  <div className='entry'>
                    <div className='entry-name'>{data.name}</div>
                    <div className='seed-type'>{data.type}</div>
                  </div>
                </MenuItem>
              )
            }
          </Menu>
        </Popover>
      </div>
    )
  }
}

export default AutoComplete
