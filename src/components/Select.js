import React from 'react'
import { SelectField, MenuItem } from 'material-ui'

const Select = props => {
  const { values, items, title, onChange, multiple } = props

  const value = multiple
    ? values
    : values && values.reduce((_, v) => v, null)

  return <SelectField
    floatingLabelText={title}
    value={value}
    onChange={onChange || function () {}}
    multiple={multiple}
  >
    {
      items.map(item =>
        <MenuItem
          key={item.id}
          value={item.id}
          primaryText={item.name}
          checked={values && values.indexOf(item.id) > -1}
        />
      )
    }
  </SelectField>
}

export default Select
