import React from 'react'
import { FontIcon } from 'material-ui'

const SelectedSeed = props =>
  <div className='seed'>
    <div className='remove' onClick={props.onRemove(props.id) || function () {}}>
      <FontIcon style={{fontSize: '12px'}} className='fas fa-times-circle' />
    </div>
    <div className='seed-type'>{props.type}</div>
    <div className='primary'>{props.primary}</div>
    <div className='secondary'>{props.secondary}</div>
  </div>

export default SelectedSeed
