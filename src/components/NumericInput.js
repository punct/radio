import React from 'react'
import { Slider } from 'material-ui'

const NumericInput = props =>
  <Slider
    min={props.min}
    max={props.max}
    step={props.step || 1}
    value={props.value}
    onChange={props.onChange || function () {}}
  />

// const NumericInput = props =>
//   <input
//     type='number'
//     min={props.min}
//     max={props.max}
//     defaultValue={props.defaultValue}
//     onChange={props.onChange || function () {}}
//   />

export default NumericInput
