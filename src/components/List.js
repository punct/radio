import React from 'react'
import { List, ListItem } from 'material-ui'
import FontIcon from 'material-ui/FontIcon'

const MyList = props =>
  <List>
    {props.items.map(item =>
      <ListItem
        key={item.id}
        primaryText={item.name}
        leftIcon={(item.selected)
          ? <h1>></h1>
          // ? <FontIcon className='fas fa-play' />
          : null}
        onClick={props.onClick(item.id)}
      />
    )}
  </List>

export default MyList
