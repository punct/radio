import { handleActions } from 'redux-actions'

import {
  CHANGE_POPULARITY,
  CHANGE_SIMILARITY,
  CHANGE_MOOD,
  MOODS_RESPONSE
} from '../actions'

const tunersReducer = initialState => handleActions({
  [CHANGE_POPULARITY]: (prevState = {}, action) => {
    return {
      ...prevState,
      popularity: action.payload
    }
  },
  [CHANGE_SIMILARITY]: (prevState = {}, action) => {
    return {
      ...prevState,
      similarity: action.payload
    }
  },
  [MOODS_RESPONSE]: (prevState = {}, action) => {
    return {
      ...prevState,
      moods: action.payload
    }
  },
  [CHANGE_MOOD]: (prevState = {}, action) => {
    return {
      ...prevState,
      moods: prevState.moods.map(({ id, name }) => ({
        id,
        name,
        selected: (action.payload.indexOf(id) > -1)
      }))
    }
  }
}, initialState)

export default tunersReducer
