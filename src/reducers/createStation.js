import { handleActions } from 'redux-actions'

import {
  OPEN_CREATE_STATION,
  CLOSE_CREATE_STATION,
  TOGGLE_CREATE_STATION
} from '../actions'

const createStationReducers = initialState => handleActions({
  [OPEN_CREATE_STATION]: (prevState, action) => {
    return {
      ...prevState,
      open: true
    }
  },
  [CLOSE_CREATE_STATION]: (prevState, action) => {
    return {
      ...prevState,
      open: false
    }
  },
  [TOGGLE_CREATE_STATION]: (prevState, action) => {
    return {
      ...prevState,
      open: !prevState.open
    }
  }
}, initialState)

export default createStationReducers
