import { handleActions } from 'redux-actions'

import {
  CHANGE_OUTPUT
} from '../actions'

const outputsReducer = initialState => handleActions({
  [CHANGE_OUTPUT]: (prevState = {}, action) => {
    return prevState
      .map(({ id, name }) => ({
        id,
        name,
        selected: (action.payload === id)
      }))
  }
}, initialState)

export default outputsReducer
