import { combineReducers } from 'redux'

import initializeReducer from './initialize'
import createStationReducer from './createStation'
import outputsReducer from './outputs'
import playbackReducer from './playback'
import stationsReducer from './stations'
import tunersReducer from './tuners'

const defaultState = {
  initialized: false,
  createStation: {
    open: false
  },
  outputs: [
    { id: 'local', name: 'Local' },
    { id: 'punct-phone', name: 'punct phone' }
  ],
  playback: {
    queue: [
      { id: 1, artist: 'Insomnium', track: 'While We Sleep' },
      { id: 2, artist: 'The Agonist', track: 'Waiting Out The Winter' },
      { id: 3, artist: 'Borknagar', track: 'Panorama' }
    ]
  },
  stations: [
    { id: 1, name: 'Playlist 1' },
    { id: 2, name: 'Playlist 2' },
    { id: 3, name: 'Playlist 3' }
  ],
  tuners: {
    popularity: 500,
    similarity: 500,
    moods: [
      { id: '65322', name: 'Peaceful' },
      { id: '65323', name: 'Romantic' },
      { id: '65324', name: 'Sentimental' },
      { id: '42942', name: 'Tender' }
    ]
  }
}

const reducers = combineReducers({
  initialized: initializeReducer(defaultState.initialized),
  createStation: createStationReducer(defaultState.createStation),
  outputs: outputsReducer(defaultState.outputs),
  playback: playbackReducer(defaultState.playback),
  stations: stationsReducer(defaultState.stations),
  tuners: tunersReducer(defaultState.tuners)
})

export default reducers
