import { handleActions } from 'redux-actions'

import {
  LOOKAHEAD_REQUEST,
  LOOKAHEAD_SUCCESS
} from '../actions'

const playbackReducer = initialState => handleActions({
  [LOOKAHEAD_REQUEST]: (prevState, action) => {
    return {
      ...prevState,
      loading: true
    }
  },
  [LOOKAHEAD_SUCCESS]: (prevState, action) => {
    return {
      ...prevState,
      loading: false,
      queue: action.payload.playlist
    }
  }
}, initialState)

export default playbackReducer
