import { handleActions } from 'redux-actions'

import {
  INITIALIZE_RESPONSE
} from '../actions'

const initializeReducer = initialState => handleActions({
  [INITIALIZE_RESPONSE]: (prevState = {}, action) => {
    return true
  }
}, initialState)

export default initializeReducer
