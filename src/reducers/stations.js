import { handleActions } from 'redux-actions'

import {
  INITIALIZE_RESPONSE,
  SELECT_PLAYLIST
} from '../actions'

const stationsReducer = initialState => handleActions({
  [INITIALIZE_RESPONSE]: (prevState = {}, action) => {
    return action.payload.stations
  },
  [SELECT_PLAYLIST]: (prevState = {}, action) => {
    return prevState
      .map(({ id, name }) => ({
        id,
        name,
        selected: (action.payload === id)
      }))
  }
}, initialState)

export default stationsReducer
