import xs from 'xstream'
import sampleCombine from 'xstream/extra/sampleCombine'

import { CONNECTION_OPEN } from './lib/wsDriver'
import {
  INITIALIZE_REQUEST,
  MOODS_REQUEST,
  SELECT_PLAYLIST,
  CHANGE_POPULARITY,
  CHANGE_SIMILARITY,
  CHANGE_MOOD,
  lookaheadRequest,
  settingsRequest
} from './actions'

const main = sources => {
  const state$ = sources.STATE

  const connectionOpen$ = sources.WS.filter(msg => msg.type === CONNECTION_OPEN)

  const initializeRequest$ = connectionOpen$
    .mapTo(({ type: INITIALIZE_REQUEST }))

  const moodsRequest$ = connectionOpen$
    .mapTo({ type: MOODS_REQUEST })

  const lookaheadRequest$ = sources.ACTION.filter(action => action.type === SELECT_PLAYLIST)
    .map(request => lookaheadRequest({
      station: request.payload
    }))

  const changePopularity$ = sources.ACTION.filter(action => action.type === CHANGE_POPULARITY)
    .compose(sampleCombine(state$))
    .map(([action, state]) => settingsRequest({
      station: state.stations.filter(station => station.selected).reduce((acc, station) => station.id, null),
      popularity: action.payload
    }))

  const changeSimilarity$ = sources.ACTION.filter(action => action.type === CHANGE_SIMILARITY)
    .compose(sampleCombine(state$))
    .map(([action, state]) => settingsRequest({
      station: state.stations.filter(station => station.selected).reduce((acc, station) => station.id, null),
      similarity: action.payload
    }))

  const changeMood$ = sources.ACTION.filter(action => action.type === CHANGE_MOOD)
    .compose(sampleCombine(state$))
    .map(([action, state]) => settingsRequest({
      station: state.stations.filter(station => station.selected).reduce((acc, station) => station.id, null),
      moods: action.payload
    }))

  const messagesSended$ = xs.merge(
    initializeRequest$,
    moodsRequest$,
    lookaheadRequest$,
    changePopularity$
    // changeSimilarity$
  )

  const action$ = xs.merge(
    sources.WS,
    lookaheadRequest$,
    changePopularity$,
    changeSimilarity$,
    changeMood$
  )

  return {
    ACTION: action$,
    WS: messagesSended$
  }
}

export default main
