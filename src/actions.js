import { createAction } from 'redux-actions'

export const INITIALIZE_REQUEST = 'INITIALIZE_REQUEST'
export const INITIALIZE_RESPONSE = 'INITIALIZE_RESPONSE'
export const CHANGE_OUTPUT = 'CHANGE_OUTPUT'
export const OPEN_CREATE_STATION = 'OPEN_CREATE_STATION'
export const CLOSE_CREATE_STATION = 'CLOSE_CREATE_STATION'
export const TOGGLE_CREATE_STATION = 'TOGGLE_CREATE_STATION'
export const CREATE_STATION = 'CREATE_STATION'
export const SELECT_PLAYLIST = 'SELECT_PLAYLIST'
export const CHANGE_POPULARITY = 'CHANGE_POPULARITY'
export const CHANGE_SIMILARITY = 'CHANGE_SIMILARITY'
export const CHANGE_MOOD = 'CHANGE_MOOD'
export const MOODS_REQUEST = 'MOODS_REQUEST'
export const MOODS_RESPONSE = 'MOODS_RESPONSE'
export const LOOKAHEAD_REQUEST = 'LOOKAHEAD_REQUEST'
export const LOOKAHEAD_SUCCESS = 'LOOKAHEAD_SUCCESS'
export const SETTING_REQUEST = 'SETTING_REQUEST'

export const initializeRequest = createAction(INITIALIZE_REQUEST)
export const changeOutput = createAction(CHANGE_OUTPUT)
export const openCreateStation = createAction(OPEN_CREATE_STATION)
export const closeCreateStation = createAction(CLOSE_CREATE_STATION)
export const toggleCreateStation = createAction(TOGGLE_CREATE_STATION)
export const createStation = createAction(CREATE_STATION)
export const selectPlaylist = createAction(SELECT_PLAYLIST)
export const changePopularity = createAction(CHANGE_POPULARITY)
export const changeSimilarity = createAction(CHANGE_SIMILARITY)
export const changeMood = createAction(CHANGE_MOOD)
export const moodRequest = createAction(MOODS_REQUEST)
export const lookaheadRequest = createAction(LOOKAHEAD_REQUEST)
export const settingsRequest = createAction(SETTING_REQUEST)
