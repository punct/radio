import React, { Component } from 'react'
import { connect } from 'react-redux'

import Tuners from './Tuners'
import Player from '../components/Player'

class CurrentlyPlaying extends Component {
  render () {
    const { playlistSelected, queueLoading, queue } = this.props

    return (
      (playlistSelected)
        ? <div className='currently-playing'>
          <Player />
          {
          queueLoading
            ? <div className='loading'>Loading</div>
            : <div className='queue'>
              {
                queue.map(song =>
                  <div className='queue-song' key={song.id}>
                    <span className='artist'>{song.artist}</span> - <span className='track'>{song.track}</span>
                  </div>
                )
              }
            </div>
        }
          <Tuners />
        </div>
        : null
    )
  }
}

export default connect(
  state => ({
    playlistSelected: state.stations.filter(station => station.selected).reduce((acc, station) => station, null),
    queueLoading: state.playback.loading,
    queue: state.playback.queue
  }),
  {
  }
)(CurrentlyPlaying)
