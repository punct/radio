import React, { Component } from 'react'
import { connect } from 'react-redux'

import Sidebar from './Sidebar'
import CurrentlyPlaying from './CurrentlyPlaying'
import CreateStation from './CreateStation'

class App extends Component {
  render () {
    return (
      (this.props.initialized)
        ? <div className='app'>
          <Sidebar />

          <CurrentlyPlaying />

          <CreateStation />
        </div>
        : <div className='loading'>Loading</div>
    )
  }
}

export default connect(
  state => ({
    initialized: state.initialized
  }),
  {}
)(App)
