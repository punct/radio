import React, { Component } from 'react'
import { connect } from 'react-redux'

import { RaisedButton } from 'material-ui'
import FontIcon from 'material-ui/FontIcon'

import Select from '../components/Select'
import List from '../components/List'

import {
  changeOutput,
  selectPlaylist,
  openCreateStation
} from '../actions'

class Sidebar extends Component {
  constructor (props) {
    super(props)

    this.onChangeOutput = this.onChangeOutput.bind(this)
    this.onSelectPlaylist = this.onSelectPlaylist.bind(this)
    this.onOpenCreateStation = this.onOpenCreateStation.bind(this)
  }

  onChangeOutput (event, index, value) {
    const { changeOutput } = this.props
    event.preventDefault()
    return changeOutput(value)
  }

  onSelectPlaylist (id) {
    const { selectPlaylist } = this.props
    return event => selectPlaylist(id)
  }

  onOpenCreateStation () {
    const { openCreateStation } = this.props
    return openCreateStation()
  }

  render () {
    const { outputs, stations } = this.props

    return (
      <div className='sidebar'>
        <div className='output'>
          <Select
            title='Output'
            items={outputs}
            value={outputs
              .filter(output => output.selected)
              .map(output => output.id)
              .reduce((acc, output) => output, null)}
            onChange={this.onChangeOutput}
          />
        </div>
        <div className='stations'>
          <List
            items={stations}
            onClick={this.onSelectPlaylist}
          />
        </div>
        <RaisedButton
          label='Create station'
          labelPosition='after'
          icon={<FontIcon className='fas fa-plus' />}
          onClick={this.onOpenCreateStation}
        />
      </div>
    )
  }
}

export default connect(
  state => ({
    outputs: state.outputs,
    stations: state.stations
  }),
  {
    changeOutput,
    selectPlaylist,
    openCreateStation
  }
)(Sidebar)
