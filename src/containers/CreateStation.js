import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Dialog,
  FlatButton,
  TextField,
  GridList
} from 'material-ui'

import SelectedSeed from '../components/SelectedSeed'
import AutoComplete from '../components/AutoComplete'
import { createStation, closeCreateStation } from '../actions'

const seeds = [
  { id: 1, name: 'Draconian', type: 'artist' },
  { id: 2, name: 'Welcome Home - Coheed and Cambria', type: 'track' },
  { id: 3, name: 'Borknagar', type: 'artist' },
  { id: 4, name: 'Metal', type: 'genre' },
  { id: 5, name: 'Be\'Lakor', type: 'artist' }
]

class CreateStationModal extends Component {
  constructor (props) {
    super(props)

    this.createStation = this.createStation.bind(this)
    this.addSeed = this.addSeed.bind(this)
    this.onChangeName = this.onChangeName.bind(this)
    this.onChangeSeed = this.onChangeSeed.bind(this)
    this.onCreateStationClose = this.onCreateStationClose.bind(this)
    this.onAddSeed = this.onAddSeed.bind(this)
    this.onRemoveSeed = this.onRemoveSeed.bind(this)

    this.state = {
      name: '',
      currentSeed: '',
      seeds: []
    }
  }

  createStation (event) {
    event.preventDefault()
    const { createStation } = this.props
    const { name, seeds } = this.state
    createStation({ name, seeds })
  }

  addSeed (event) {
    event.preventDefault()
    this.setState({
      seeds: [...this.state.seeds, this.state.currentSeed]
    })
  }

  onChangeName (event, value) {
    this.setState({
      name: value
    })
  }

  onChangeSeed (event) {
    this.setState({
      currentSeed: event.target.value
    })
  }

  onCreateStationClose () {
    const { closeCreateStation } = this.props
    return closeCreateStation()
  }

  onAddSeed (value) {
    this.setState({
      seeds: [...this.state.seeds, value]
    })
  }

  onRemoveSeed (value) {
    return event => {
      this.setState({
        seeds: this.state.seeds.filter(seed => seed.id !== value)
      })
    }
  }

  render () {
    const { createStationOpened } = this.props

    const actions = [
      <FlatButton
        label='Cancel'
        onClick={this.onCreateStationClose}
      />,
      <FlatButton
        label='Submit'
        primary
        onClick={this.createStation}
      />
    ]

    return (
      <Dialog
        title='Create Station'
        actions={actions}
        modal={false}
        open={createStationOpened}
        onRequestClose={this.onCreateStationClose}
        className='create-station-modal'
      >
        <TextField
          floatingLabelText='Name'
          defaultValue={this.state.name}
          onChange={this.onChangeName}
        />
        <AutoComplete
          hintText='Type an artist, a track or a genre'
          floatingLabelText='Seed'
          dataSource={seeds}
          dataSourceConfig={{ text: 'name', value: 'id' }}
          filter={AutoComplete.caseInsensitiveFilter}
          onItemSelect={this.onAddSeed}
        />
        <GridList cols={5.1}>
          {
            this.state.seeds.map(seed => {
              const primary = (seed.type === 'track') ? seed.name.split(' - ')[0] : seed.name
              const secondary = (seed.type === 'track') ? seed.name.split(' - ')[1] : ''
              return <SelectedSeed
                key={seed.id}
                id={seed.id}
                type={seed.type}
                primary={primary}
                secondary={secondary}
                onRemove={this.onRemoveSeed}
              />
            })
          }
        </GridList>
      </Dialog>
    )
  }
}

export default connect(
  state => ({
    createStationOpened: state.createStation.open
  }),
  {
    createStation,
    closeCreateStation

  }
)(CreateStationModal)
