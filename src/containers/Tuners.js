import React, { Component } from 'react'
import { connect } from 'react-redux'

import NumericInput from '../components/NumericInput'
import Select from '../components/Select'

import {
  changePopularity,
  changeSimilarity,
  changeMood
} from '../actions'

class Tuners extends Component {
  constructor (props) {
    super(props)

    this.onChangePopularity = this.onChangePopularity.bind(this)
    this.onChangeSimilarity = this.onChangeSimilarity.bind(this)
    this.onChangeMood = this.onChangeMood.bind(this)
  }

  onChangePopularity (event, value) {
    const { changePopularity } = this.props
    event.preventDefault()
    return changePopularity(value)
  }

  onChangeSimilarity (event, value) {
    const { changeSimilarity } = this.props
    event.preventDefault()
    return changeSimilarity(value)
  }

  onChangeMood (event, index, value) {
    const { changeMood } = this.props
    event.preventDefault()
    return changeMood(value)
  }

  render () {
    const { moods, popularity, similarity } = this.props
    const selectedMoodIds = moods
      .filter(mood => mood.selected)
      .map(mood => mood.id)

    return (
      <div className='tuners'>
        <div className='popularity'>
          Popularity:
          <NumericInput
            min={0}
            max={1000}
            value={popularity}
            onChange={this.onChangePopularity}
          />
        </div>
        <div className='similarity'>
          Similarity:
          <NumericInput
            min={0}
            max={1000}
            value={similarity}
            onChange={this.onChangeSimilarity}
          />
        </div>
        <div className='moods'>
          <Select
            title='Mood'
            items={moods}
            values={selectedMoodIds}
            onChange={this.onChangeMood}
            multiple
          />
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    popularity: state.tuners.popularity,
    similarity: state.tuners.similarity,
    moods: state.tuners.moods
  }),
  {
    changePopularity,
    changeSimilarity,
    changeMood
  }
)(Tuners)
